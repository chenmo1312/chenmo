echo '<meta charset="utf-8">' > $1.html
cat $1 >> $1.html
echo '<link rel="stylesheet" href="'$2'">' >> $1.html
echo '<style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style>' >> $1.html
echo '<script src="'$HOME'/bin/markdeep/markdeep.min.js" charset="utf-8"></script>' >> $1.html
echo '<script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script>' >> $1.html
echo '<script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>' >> $1.html
